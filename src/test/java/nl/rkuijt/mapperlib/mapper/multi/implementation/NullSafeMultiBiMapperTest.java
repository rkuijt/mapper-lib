package nl.rkuijt.mapperlib.mapper.multi.implementation;

import nl.rkuijt.mapperlib.mapper.multi.BaseMultiBiMapperTest;
import nl.rkuijt.mapperlib.mapper.multi.nullsafe.NullSafeMultiBiMapper;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings({"Convert2Lambda", "RedundantSuppression"}) // Do not recommend converting anonymous class to lambda as lambdas are final and cannot be mocked.
class NullSafeMultiBiMapperTest extends BaseMultiBiMapperTest<String, Long> {

	NullSafeMultiBiMapperTest() {
		sut = new NullSafeMultiBiMapper<>() {
			@Override
			public Long nonNullFrom(final String input) {
				assertThat(input).isNotNull();
				return 1L;
			}

			@Override
			public String nonNullReverse(final Long input) {
				assertThat(input).isNotNull();
				return "1";
			}
		};

		fromInput = new ArrayList<>();
		fromInput.add("1");
		fromInput.add(null);
		fromInput.add("3");

		fromExpected = new ArrayList<>();
		fromExpected.add(1L);
		fromExpected.add(null);
		fromExpected.add(1L);

		reverseInput = new ArrayList<>();
		reverseInput.add(1L);
		reverseInput.add(null);
		reverseInput.add(3L);

		reverseExpected = new ArrayList<>();
		reverseExpected.add("1");
		reverseExpected.add(null);
		reverseExpected.add("1");
	}
}

package nl.rkuijt.mapperlib.mapper.multi.implementation;

import nl.rkuijt.mapperlib.mapper.multi.BaseMultiBiMapperTest;
import nl.rkuijt.mapperlib.mapper.multi.MultiBiMapper;

import java.util.List;

@SuppressWarnings({"Convert2Lambda", "RedundantSuppression"}) // Do not recommend converting anonymous class to lambda as lambdas are final and cannot be mocked.
class MultiBiMapperTest extends BaseMultiBiMapperTest<String, Long> {

	MultiBiMapperTest() {
		sut = new MultiBiMapper<>() {
			@Override
			public Long from(final String input) {
				return 1L;
			}

			@Override
			public String reverse(final Long input) {
				return "1";
			}
		};

		fromInput = List.of("1", "2", "3");
		fromExpected = List.of(1L, 1L, 1L);
		reverseInput = List.of(1L, 2L, 3L);
		reverseExpected = List.of("1", "1", "1");
	}
}


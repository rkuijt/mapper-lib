package nl.rkuijt.mapperlib.mapper.multi.implementation;

import nl.rkuijt.mapperlib.mapper.multi.BaseMultiMapperTest;
import nl.rkuijt.mapperlib.mapper.multi.MultiMapper;

import java.util.List;

@SuppressWarnings({"Convert2Lambda", "RedundantSuppression"}) // Do not recommend converting anonymous class to lambda as lambdas are final and cannot be mocked.
class MultiMapperTest extends BaseMultiMapperTest<String, Long> {

	MultiMapperTest() {
		sut = new MultiMapper<>() {
			@Override
			public Long from(final String input) {
				return 1L;
			}
		};

		fromInput = List.of("1", "2", "3");
		fromExpected = List.of(1L, 1L, 1L);
	}
}

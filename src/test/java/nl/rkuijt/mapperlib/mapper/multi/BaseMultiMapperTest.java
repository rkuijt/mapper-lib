package nl.rkuijt.mapperlib.mapper.multi;

import lombok.Getter;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PROTECTED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Generic tests for Multi mappers, see {@link nl.rkuijt.mapperlib.mapper.multi.implementation} for test implementations.
 */
public abstract class BaseMultiMapperTest<I, O> {
	protected MultiMapper<I, O> sut;
	protected Collection<O> fromExpected;
	protected Collection<I> fromInput;
	@Getter(PROTECTED)
	protected MultiMapper<I, O> sutSpy;
	private final Collector<O, ?, List<O>> fromCollector = toList();

	static String fieldMissingMessage(final String field) {
		return String.format("The field '%s' should be set in order to run this test.", field);
	}

	@BeforeEach
	public void beforeEach() {
		requireNonNull(sut, fieldMissingMessage("sut"));
		sutSpy = spy(sut);
		requireNonNull(sutSpy, "Something went wrong with initializing a the spy for this test.");
		requireNonNull(fromInput, fieldMissingMessage("fromInput"));
		requireNonNull(fromCollector, fieldMissingMessage("fromCollector"));
		requireNonNull(fromExpected, fieldMissingMessage("fromExpected"));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	@DisplayName("from: Throw exception when collection parameter is null.")
	void given_Instance_when_collectionNull_then_fromShouldThrowException() {
		final ThrowableAssert.ThrowingCallable operation = () -> sutSpy.from(null, fromCollector);

		assertThatThrownBy(operation)
				.isInstanceOf(NullPointerException.class)
				.hasMessage("inputs is marked non-null but is null");

		verify(sutSpy, never())
				.from(any());
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	@DisplayName("from: Throw exception when collector parameter is null.")
	void given_Instance_when_collectorNull_then_fromShouldThrowException() {
		final ThrowableAssert.ThrowingCallable operation = () -> sutSpy.from(fromInput, null);

		assertThatThrownBy(operation)
				.isInstanceOf(NullPointerException.class)
				.hasMessage("collector is marked non-null but is null");

		verify(sutSpy, never())
				.from(any());
	}

	@Test
	@DisplayName("from: Succeed on valid input.")
	void given_Instance_when_validInput_then_fromShouldYieldCorrectResult() {
		final Collection<O> actual = sutSpy.from(fromInput, fromCollector);

		assertThat(actual).isEqualTo(fromExpected);

		verify(sutSpy, times(3))
				.from(any());
	}
}

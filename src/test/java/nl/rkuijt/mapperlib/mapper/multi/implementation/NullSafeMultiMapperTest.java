package nl.rkuijt.mapperlib.mapper.multi.implementation;

import nl.rkuijt.mapperlib.mapper.multi.BaseMultiMapperTest;
import nl.rkuijt.mapperlib.mapper.multi.nullsafe.NullSafeMultiMapper;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings({"Convert2Lambda", "RedundantSuppression"}) // Do not recommend converting anonymous class to lambda as lambdas are final and cannot be mocked.
class NullSafeMultiMapperTest extends BaseMultiMapperTest<String, Long> {

	NullSafeMultiMapperTest() {
		sut = new NullSafeMultiMapper<>() {
			@Override
			protected Long nonNullFrom(final String input) {
				assertThat(input).isNotNull();
				return 1L;
			}
		};

		// List.of() doesn't take null values.
		fromInput = new ArrayList<>();
		fromInput.add("1");
		fromInput.add(null);
		fromInput.add("3");

		fromExpected = new ArrayList<>();
		fromExpected.add(1L);
		fromExpected.add(null);
		fromExpected.add(1L);
	}
}

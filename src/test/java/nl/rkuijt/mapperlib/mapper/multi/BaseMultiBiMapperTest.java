package nl.rkuijt.mapperlib.mapper.multi;

import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

/**
 * Generic tests for BiMulti mappers, see {@link nl.rkuijt.mapperlib.mapper.multi.implementation} for test implementations.
 *
 * @see BaseMultiMapperTest
 */
public abstract class BaseMultiBiMapperTest<I, O> extends BaseMultiMapperTest<I, O> {
	protected Collection<I> reverseExpected;
	protected Collection<O> reverseInput;
	private final Collector<I, ?, List<I>> reverseCollector = toList();

	@Override
	@BeforeEach
	public void beforeEach() {
		super.beforeEach();
		requireNonNull(reverseInput, fieldMissingMessage("reverseInput"));
		requireNonNull(reverseCollector, fieldMissingMessage("reverseCollector"));
		requireNonNull(fromExpected, fieldMissingMessage("reverseExpected"));
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	@DisplayName("reverse: Throw exception when collection parameter is null.")
	void given_Instance_when_collectionNull_then_reverseShouldThrowException() {
		final ThrowableAssert.ThrowingCallable operation = () -> getSutSpy().reverse(null, reverseCollector);

		assertThatThrownBy(operation)
				.isInstanceOf(NullPointerException.class)
				.hasMessage("inputs is marked non-null but is null");

		verify(getSutSpy(), never())
				.reverse(any());
	}

	@Test
	@SuppressWarnings("ConstantConditions")
	@DisplayName("reverse: Throw exception when collector parameter is null.")
	void given_Instance_when_collectorNull_then_reverseShouldThrowException() {
		final ThrowableAssert.ThrowingCallable operation = () -> getSutSpy().reverse(reverseInput, null);

		assertThatThrownBy(operation)
				.isInstanceOf(NullPointerException.class)
				.hasMessage("collector is marked non-null but is null");

		verify(getSutSpy(), never())
				.reverse(any());
	}

	@Test
	@DisplayName("reverse: Succeed on valid input.")
	void given_Instance_when_validInput_then_reverseShouldYieldCorrectResult() {
		final Collection<I> actual = getSutSpy().reverse(reverseInput, reverseCollector);

		assertThat(actual)
				.isEqualTo(reverseExpected);

		verify(getSutSpy(), times(3))
				.reverse(any());
	}

	@Override
	protected MultiBiMapper<I, O> getSutSpy() {
		return (MultiBiMapper<I, O>) super.getSutSpy();
	}
}

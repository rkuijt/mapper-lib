package nl.rkuijt.mapperlib.mapper.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

/**
 * This class serves as a cached classloader for architecture tests.
 * <p>
 * Rationale:
 * ArchUnit includes neat annotations in their archunit-junit5-api library which include features like caching.
 * At the time of writing, these annotations do not seem to be compatible with parameterized tests.
 * As this class is initialized as singleton, caching is still provided and classloading configuration is encapsulated.
 */
@NoArgsConstructor(access = PRIVATE)
class ArchitectureTestContext {
	private static final String ROOT_PACKAGE = "nl.rkuijt";
	private static ArchitectureTestContext instance;
	@Getter
	private final JavaClasses suts = new ClassFileImporter().withImportOption(new ImportOption.DoNotIncludeTests())
	                                                        .importPackages(ROOT_PACKAGE);

	synchronized static ArchitectureTestContext getInstance() {
		if (instance == null) {
			instance = new ArchitectureTestContext();
		}
		return instance;
	}
}

package nl.rkuijt.mapperlib.mapper.architecture;

import com.tngtech.archunit.lang.syntax.elements.ClassesShouldConjunction;
import nl.rkuijt.mapperlib.mapper.multi.MultiMapper;
import nl.rkuijt.mapperlib.mapper.single.BiMapper;
import nl.rkuijt.mapperlib.mapper.single.Mapper;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

/**
 * Responsible for checking naming conventions.
 */
class MapperNamingConventionTest {
	private final ArchitectureTestContext testContext = ArchitectureTestContext.getInstance();

	private static Stream<Arguments> namingConventionBindings() {
		return Stream.of(
				Arguments.of(Mapper.class, "^.*Mapper$"),
				Arguments.of(BiMapper.class, "^.*BiMapper$"),
				Arguments.of(MultiMapper.class, "^.*Multi.*Mapper$")
		);
	}

	@MethodSource("namingConventionBindings")
	@ParameterizedTest(name = "has a name matching {1} so must be assignable to {0}")
	void shouldBeAssignableTo(final Class<?> targetClass, final String matcher) {
		final ClassesShouldConjunction rule = classes().that().haveNameMatching(matcher)
		                                               .should().beAssignableTo(targetClass);
		rule.check(testContext.getSuts());
	}

	@MethodSource("namingConventionBindings")
	@ParameterizedTest(name = "is assignable to {0} so must have a name matching {1}")
	void shouldHaveNameMatching(final Class<?> targetClass, final String matcher) {
		final ClassesShouldConjunction rule = classes().that().areAssignableTo(targetClass)
		                                               .should().haveNameMatching(matcher);
		rule.check(testContext.getSuts());
	}
}

package nl.rkuijt.mapperlib.mapper.single.nullsafe;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class NullSafeBiMapperTest {
	private final NullSafeBiMapper<String, Long> sut = new NullSafeBiMapper<>() {
		@Override
		protected Long nonNullFrom(final String input) {
			return 1L;
		}

		@Override
		protected String nonNullReverse(final Long input) {
			return "1";
		}
	};

	@Test
	@SuppressWarnings("ConstantConditions")
	@DisplayName("Throw exception when input is null.")
	void given_Instance_when_inputNull_then_expectNull() {
		final Long input = null;

		assertThat(sut.reverse(input))
				.isNull();
	}

	@Test
	@DisplayName("Succeed on valid input.")
	void given_Instance_when_inputValid_then_expectSuccessfulMapping() {
		final Long input = 1L;
		final String expected = "1";

		assertThat(sut.reverse(input))
				.isEqualTo(expected);
	}
}

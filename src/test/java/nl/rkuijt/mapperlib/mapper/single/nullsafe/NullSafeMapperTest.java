package nl.rkuijt.mapperlib.mapper.single.nullsafe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class NullSafeMapperTest {
	private final NullSafeMapper<String, Long> sut = new NullSafeMapper<>() {
		@Override
		protected Long nonNullFrom(final String input) {
			return 1L;
		}
	};

	@Test
	void given_Instance_when_inputNull_then_expectNull() {
		assertThat(sut.from(null))
				.isNull();
	}

	@Test
	void given_Instance_when_inputValid_then_expectSuccessfulMapping() {
		final String input = "1";
		final long expected = 1L;

		assertThat(sut.from(input))
				.isEqualTo(expected);
	}
}

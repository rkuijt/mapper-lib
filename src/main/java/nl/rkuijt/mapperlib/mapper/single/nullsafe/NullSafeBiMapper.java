package nl.rkuijt.mapperlib.mapper.single.nullsafe;

import nl.rkuijt.mapperlib.mapper.single.BiMapper;

import static java.util.Optional.ofNullable;

/**
 * Null-safe bidirectional type mapper.
 *
 * @param <F> the type to map from.
 * @param <T> the type to map to.
 * @see NullSafeMapper
 * @see BiMapper
 */
public abstract class NullSafeBiMapper<F, T> extends NullSafeMapper<F, T> implements BiMapper<F, T> {

	/**
	 * Maps the {@code to} type to the {@code from} type.
	 * Does not pass null references to the mapping method.
	 *
	 * @param input {@code to} object.
	 * @return {@code from} object. {@code null} if {@code input} is {@code null}.
	 */
	@Override
	public final F reverse(final T input) {
		return ofNullable(input)
				.map(this::nonNullReverse)
				.orElse(null);
	}

	/**
	 * Maps the {@code to} type to the {@code from} type.
	 *
	 * @param input {@code to} object.
	 * @return {@code from} object.
	 * @see BiMapper#reverse(Object)
	 */
	protected abstract F nonNullReverse(final T input);
}

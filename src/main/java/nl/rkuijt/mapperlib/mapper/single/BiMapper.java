package nl.rkuijt.mapperlib.mapper.single;

/**
 * Bidirectional type mapper.
 *
 * @param <F> the type to map from.
 * @param <T> the type to map to.
 * @see Mapper
 */
public interface BiMapper<F, T> extends Mapper<F, T> {

	/**
	 * Maps the {@code to} type to the {@code from} type.
	 * Null references must be handled by the mapping implementation.
	 *
	 * @param input {@code to} object.
	 * @return {@code from} object.
	 */
	F reverse(T input);
}

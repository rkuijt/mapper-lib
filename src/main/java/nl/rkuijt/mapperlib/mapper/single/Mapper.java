package nl.rkuijt.mapperlib.mapper.single;

/**
 * Unidirectional type mapper.
 *
 * @param <F> the type to map from.
 * @param <T> the type to map to.
 */
public interface Mapper<F, T> {

	/**
	 * Maps the {@code from} type to the {@code to} type.
	 * Null references must be handled by the mapping implementation.
	 *
	 * @param input {@code from} object.
	 * @return {@code to} object.
	 */
	T from(F input);
}

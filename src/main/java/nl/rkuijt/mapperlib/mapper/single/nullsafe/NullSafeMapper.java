package nl.rkuijt.mapperlib.mapper.single.nullsafe;

import nl.rkuijt.mapperlib.mapper.single.Mapper;

import static java.util.Optional.ofNullable;

/**
 * Null-safe unidirectional type mapper.
 *
 * @param <F> the type to map from.
 * @param <T> the type to map to.
 * @see Mapper
 */
public abstract class NullSafeMapper<F, T> implements Mapper<F, T> {

	/**
	 * Maps the {@code from} type to the {@code to} type.
	 * Does not pass null references to the mapping method.
	 *
	 * @param input {@code from} object.
	 * @return {@code to} object. {@code null} if {@code input} is {@code null}.
	 */
	@Override
	public final T from(final F input) {
		return ofNullable(input)
				.map(this::nonNullFrom)
				.orElse(null);
	}

	/**
	 * Maps the {@code from} type to the {@code to} type.
	 *
	 * @param input {@code from} object.
	 * @return {@code to} object.
	 * @see Mapper#from(Object)
	 */
	protected abstract T nonNullFrom(final F input);
}

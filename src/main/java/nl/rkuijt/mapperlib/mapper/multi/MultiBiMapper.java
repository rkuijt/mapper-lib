package nl.rkuijt.mapperlib.mapper.multi;

import lombok.NonNull;
import nl.rkuijt.mapperlib.mapper.single.BiMapper;

import java.util.Collection;
import java.util.stream.Collector;

/**
 * Bidirectional Collection type mapper.
 *
 * @param <F> the type to map from.
 * @param <T> the type to map to.
 * @see MultiMapper
 * @see BiMapper
 */
public interface MultiBiMapper<F, T> extends MultiMapper<F, T>, BiMapper<F, T> {

	/**
	 * Maps Collections of the {@code to} type to Collections of the {@code from} type.
	 *
	 * @param inputs    Collection of {@code to} objects.
	 * @param collector Collector used to collect mapped objects.
	 * @param <F2>      Type of the output collection.
	 * @return Collection of {@code from} objects.
	 * @implSpec Null references must be handled by the mapping implementation.
	 * @implNote Should not be overridden.
	 */
	default <F2 extends Collection<F>> F2 reverse(@NonNull final Collection<T> inputs, @NonNull final Collector<F, ?, F2> collector) {
		return inputs.stream()
		             .map(this::reverse)
		             .collect(collector);
	}
}

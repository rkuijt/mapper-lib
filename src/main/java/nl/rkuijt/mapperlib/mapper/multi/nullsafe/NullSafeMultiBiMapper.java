package nl.rkuijt.mapperlib.mapper.multi.nullsafe;

import nl.rkuijt.mapperlib.mapper.multi.MultiBiMapper;
import nl.rkuijt.mapperlib.mapper.single.nullsafe.NullSafeBiMapper;

/**
 * Null-safe bidirectional Collection type mapper.
 *
 * @param <F> the type to map from.
 * @param <T> the type to map to.
 * @see NullSafeMultiMapper
 * @see MultiBiMapper
 */
public abstract class NullSafeMultiBiMapper<F, T> extends NullSafeBiMapper<F, T> implements MultiBiMapper<F, T> {}


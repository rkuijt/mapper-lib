package nl.rkuijt.mapperlib.mapper.multi;

import lombok.NonNull;
import nl.rkuijt.mapperlib.mapper.single.Mapper;

import java.util.Collection;
import java.util.stream.Collector;

/**
 * Unidirectional Collection type mapper.
 *
 * @param <F> the type to map from.
 * @param <T> the type to map to.
 * @see Mapper
 */
public interface MultiMapper<F, T> extends Mapper<F, T> {

	/**
	 * Maps Collections of the {@code from} type to Collections of the {@code to} type.
	 *
	 * @param inputs    Collection of {@code from} objects.
	 * @param collector Collector used to collect mapped objects.
	 * @param <T2>      Type of the output collection.
	 * @return Collection of {@code to} objects.
	 * @implSpec Null references must be handled by the mapping implementation.
	 * @implNote Should not be overridden.
	 */
	default <T2 extends Collection<T>> T2 from(@NonNull final Collection<F> inputs, @NonNull final Collector<T, ?, T2> collector) {
		return inputs.stream()
		             .map(this::from)
		             .collect(collector);
	}
}


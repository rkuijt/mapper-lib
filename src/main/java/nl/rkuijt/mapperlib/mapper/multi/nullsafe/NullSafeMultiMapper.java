package nl.rkuijt.mapperlib.mapper.multi.nullsafe;

import nl.rkuijt.mapperlib.mapper.multi.MultiMapper;
import nl.rkuijt.mapperlib.mapper.single.nullsafe.NullSafeMapper;

/**
 * Null-safe unidirectional Collection type mapper.
 *
 * @param <F> the type to map from.
 * @param <T> the type to map to.
 * @see MultiMapper
 */
public abstract class NullSafeMultiMapper<F, T> extends NullSafeMapper<F, T> implements MultiMapper<F, T> {}

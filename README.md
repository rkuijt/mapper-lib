# mapper-lib
_Clean, lightweight and simple mapping library._

[![Quality Gate Status](https://sonar.rgkuijt.nl/api/project_badges/measure?project=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40&metric=alert_status&token=c137b059de9b1103d5c894b3b6129647eddd1f4e)](https://sonar.rgkuijt.nl/dashboard?id=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40)
[![Maintainability Rating](https://sonar.rgkuijt.nl/api/project_badges/measure?project=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40&metric=sqale_rating&token=c137b059de9b1103d5c894b3b6129647eddd1f4e)](https://sonar.rgkuijt.nl/dashboard?id=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40)
[![Reliability Rating](https://sonar.rgkuijt.nl/api/project_badges/measure?project=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40&metric=reliability_rating&token=c137b059de9b1103d5c894b3b6129647eddd1f4e)](https://sonar.rgkuijt.nl/dashboard?id=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40)
[![Security Rating](https://sonar.rgkuijt.nl/api/project_badges/measure?project=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40&metric=security_rating&token=c137b059de9b1103d5c894b3b6129647eddd1f4e)](https://sonar.rgkuijt.nl/dashboard?id=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40)
[![Coverage](https://sonar.rgkuijt.nl/api/project_badges/measure?project=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40&metric=coverage&token=c137b059de9b1103d5c894b3b6129647eddd1f4e)](https://sonar.rgkuijt.nl/dashboard?id=rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40)

---

This library provides a combination of generic mapping interfaces which allow for easy mapping between types.
This kind of mapping is often helpful in order to separate logical components, for example between layers or when mapping between bounded contexts.

This library is available on [Maven Central](https://mvnrepository.com/artifact/nl.rkuijt/mapper-lib).

## Usage
There are a few notable mapping interfaces:

| Interface   | Purpose                                                                                                                                |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------|
| Mapper      | Unidirectional mapping.                                                                                                                |
| Bi...       | Bidirectional mapping.                                                                                                                 |
| Multi...    | Supports mapping collections.                                                                                                          |
| NullSafe... | Does not pass `null` values to mapping method but returns `null` directly instead. Relieves you from implementing null safety checks.  |

The `NullSafeMultiBiMapper` is the most feature rich one, combining all of the listed behavior.

### NullSafeMapper
Used to map from one object to another.
Upon receiving a null, directly return a null instead of passing it to the mapping implementation.

```java
final Mapper<FooDto, Foo> mapper = new NullSafeMapper<>() {
    @Override
    protected Foo nonNullFrom(final FooDto input) {
        return Foo.builder()
                  .id(input.getId())
                  .name(input.getName())
                  .build();
    }
};

final FooDto input = new FooDto(42, "Zaphod Beeblebrox");
final Foo output = mapper.from(input);
```

### NullSafeBiMapper
Used to map from one object to another and back.
Upon receiving a null, directly return a null instead of passing it to the mapping implementation.

```java
final BiMapper<FooDto, Foo> mapper = new NullSafeBiMapper<>() {
    @Override
    protected Foo nonNullFrom(final FooDto input) {
        return Foo.builder()
                  .id(input.getId())
                  .name(input.getName())
                  .build();
    }
    
    @Override
    protected FooDto nonNullReverse(final Foo input) {
            return FooDto.builder()
                         .id(input.getId())
                         .name(input.getName())
                         .build();
    }
};

final FooDto input = new FooDto(42, "Zaphod Beeblebrox");
final Foo output = mapper.from(input);
final FooDto andBackAgain = mapper.reverse(output);
```

### NullSafeMultiMapper
Also supports mapping collections of one type to collections of another type.
Ignores null values in the input collection instead of passing it to the mapping implementation.

```java
final MultiMapper<FooDto, Foo> mapper = new NullSafeMultiMapper<>() {
    @Override
    public Foo nonNullFrom(final FooDto input) {
        return FooDto.builder()
                     .id(input.getId())
                     .name(input.getName())
                     .build();
    }
};

final FooDto input = new FooDto(42, "Zaphod Beeblebrox");
final Foo output = mapper.from(input);

final List<FooDto> inputList = List.of(input, null, input);
final Set<Foo> outputList = mapper.from(inputList, Collectors.toSet());
```

> ℹ️ **Note:** MultiMappers extend single object mappers, so you can map both with the same mapper.

> ℹ️ **Note:** When mapping collections, you need to specify the collector inline. This allows mapping both the object and its container dynamically.

### NullSafeMultiBiMapper
Also supports mapping collections of one type to collections of another type and back.
Ignores null values in the input collection instead of passing it to the mapping implementation.

```java
final MultiBiMapper<FooDto, Foo> mapper = new NullSafeMultiBiMapper<>() {
    @Override
    public Foo nonNullFrom(final FooDto input) {
        return Foo.builder()
                  .id(input.getId())
                  .name(input.getName())
                  .build();
    }

    @Override
    public FooDto nonNullReverse(final Foo input) {
    		return FooDto.builder()
                         .id(input.getId())
                         .name(input.getName())
                         .build();
    }
};

final FooDto input = new FooDto(42, "Zaphod Beeblebrox");
final Foo output = mapper.from(input);
final FooDto andBackAgain = mapper.reverse(output);

final List<FooDto> inputList = List.of(input, null, input);
final Set<Foo> outputList = mapper.from(inputList, Collectors.toSet());
final List<FooDto> andBackAgainList = mapper.reverse(outputList, Collectors.toList());
```

> ℹ️ **Note:** MultiMappers extend single object mappers, so you can map both with the same mapper.

> ℹ️ **Note:** When mapping collections, you need to specify the collector inline. This allows mapping both the object and its container dynamically.


### Mapper
Used to map from one object to another.

```java
Mapper<FooDto, Foo> mapper = input -> Foo.builder()
                                         .id(input.getId())
                                         .name(input.getName())
                                         .build();

final FooDto input = new FooDto(42, "Zaphod Beeblebrox");
final Foo output = mapper.from(input);
```

> ⚠️ **Caution:** `Mapper` is not null-safe and will pass null to the mapping implementation. In this example, that would cause a NullPointerException.  
> It is recommended to use `NullSafeMapper` instead unless you need this behavior.

### BiMapper
Used to map from one object to another and back.

```java
final BiMapper<FooDto, Foo> mapper = new BiMapper<>() {
    @Override
    public Foo from(final FooDto input) {
        return Foo.builder()
                  .id(input.getId())
                  .name(input.getName())
                  .build();
    }

    @Override
    public FooDto reverse(final Foo input) {
        return FooDto.builder()
                     .id(input.getId())
                     .name(input.getName())
                     .build();
    }
};

final FooDto input = new FooDto(42, "Zaphod Beeblebrox");
final Foo output = mapper.from(input);
final FooDto andBackAgain = mapper.reverse(output);
```

> ⚠️ **Caution:** `BiMapper` is not null-safe and will pass null to the mapping implementation. In this example, that would cause a NullPointerException.  
> It is recommended to use `NullSafeBiMapper` instead unless you need this behavior.

### MultiMapper
Also supports mapping collections of one type to collections of another type.

```java
final MultiMapper<FooDto, Foo> mapper = new MultiMapper<>() {
    @Override
    public Foo from(final FooDto input) {
        return FooDto.builder()
                     .id(input.getId())
                     .name(input.getName())
                     .build();
    }
};

final FooDto input = new FooDto(42, "Zaphod Beeblebrox");
final Foo output = mapper.from(input);

final List<FooDto> inputList = List.of(input, null, input);
final Set<Foo> outputList = mapper.from(inputList, Collectors.toSet());
```

> ⚠️ **Caution:** `BiMapper` is not null-safe and will pass null to the mapping implementation. In this example, that would cause a NullPointerException.  
> It is recommended to use `NullSafeMultiMapper` instead unless you need this behavior.

> ℹ️ **Note:** MultiMappers extend single object mappers, so you can map both with the same mapper.

> ℹ️ **Note:** When mapping collections, you need to specify the collector inline. This allows mapping both the object and its container dynamically.

### MultiBiMapper
Also supports mapping collections of one type to collections of another type and back.

```java
final MultiBiMapper<FooDto, Foo> mapper = new MultiBiMapper<>() {
    @Override
    public Foo from(final FooDto input) {
        return Foo.builder()
                  .id(input.getId())
                  .name(input.getName())
                  .build();
    }

    @Override
    public FooDto reverse(final Foo input) {
    		return FooDto.builder()
                         .id(input.getId())
                         .name(input.getName())
                         .build();
    }
};

final FooDto input = new FooDto(42, "Zaphod Beeblebrox");
final Foo output = mapper.from(input);
final FooDto andBackAgain = mapper.reverse(output);

final List<FooDto> inputList = List.of(input, null, input);
final Set<Foo> outputList = mapper.from(inputList, Collectors.toSet());
final List<FooDto> andBackAgainList = mapper.reverse(outputList, Collectors.toList());
```

> ⚠️ **Caution:** `MultiBiMapper` is not null-safe and will pass null to the mapping implementation. In this example, that would cause a NullPointerException.  
> It is recommended to use `NullSafeMultiBiMapper` instead unless you need this behavior.

> ℹ️ **Note:** MultiMappers extend single object mappers, so you can map both with the same mapper.

> ℹ️ **Note:** When mapping collections, you need to specify the collector inline. This allows mapping both the object and its container dynamically.

---

## Contributing

### Feedback
- Feel free to create a new issue 👍 Please do search the current issues first in order to avoid duplicates.
- Please provide as much information as possible in order to debug or reproduce the issue. Be careful not to disclose sensitive information though.

### Development environment
- **Optional:** In order to generate UML diagrams for the javadoc, you need to have GraphViz installed and the `GRAPHVIZ_DOT` environment variable set to the path of its `dot` binary.

### Quality governance
This quality of this project is heavily safeguarded. This codebase has:
- 100% Branch coverage
- 100% Mutation coverage
- Architecture tests
- Security tests by Snyk
- Linting and static analysis
- Complete API documentation, [available here.](https://docs.mapper-lib.rkuijt.nl/)

## Acknowledgements

Special thanks to Leon Akkermans for co-authoring the initial mapping interfaces. 

---

Made with ❤️

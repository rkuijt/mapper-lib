import info.solidsoft.gradle.pitest.PitestPluginExtension
import org.gradle.api.JavaVersion.VERSION_11

val umlDoclet: Configuration by configurations.creating

plugins {
    `java-library`
    jacoco
    `maven-publish`
    signing
    id("info.solidsoft.pitest") version "1.7.0"
    id("org.sonarqube") version "3.3"
    id("io.snyk.gradle.plugin.snykplugin") version "0.4"
}

group = "nl.rkuijt"
version = "1.0.0"

java {
    sourceCompatibility = VERSION_11
    targetCompatibility = VERSION_11
}

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    compileOnly("org.projectlombok:lombok:1.18.22")
    annotationProcessor("org.projectlombok:lombok:1.18.22")
    testCompileOnly("org.projectlombok:lombok:1.18.22")
    testAnnotationProcessor("org.projectlombok:lombok:1.18.22")
    testImplementation("com.tngtech.archunit:archunit:0.22.0")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.8.2")
    testImplementation("org.mockito:mockito-junit-jupiter:4.3.1")
    testImplementation("org.mockito:mockito-core:4.2.0")
    testImplementation("org.assertj:assertj-core:3.22.0")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.2")
    umlDoclet("nl.talsmasoftware:umldoclet:2.0.15")
}

val isSnapshot = version.toString().endsWith("SNAPSHOT")

configurations {
    umlDoclet
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}

tasks.test {
    finalizedBy(
        listOf(
            tasks.pitest,
            tasks.jacocoTestReport
        )
    )
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    reports {
        xml.required.set(true)
    }
}

tasks.pitest {
    dependsOn(tasks.test)
}

configure<PitestPluginExtension> {
    junit5PluginVersion.set("0.15")
    outputFormats.set(listOf("XML", "HTML"))
    timestampedReports.set(false)
}

tasks.javadoc {
    source = sourceSets.main.get().allJava
    val docletOptions = options as StandardJavadocDocletOptions
    docletOptions.tags = listOf("implNote", "implSpec")
    docletOptions.docletpath = umlDoclet.files.toList()
    docletOptions.doclet = "nl.talsmasoftware.umldoclet.UMLDoclet"
    docletOptions.bottom = "<h3 style='text-align: center;'>Visit the project's source code at <a href='https://gitlab.com/rkuijt/mapper-lib/'>GitLab</a></h3>"
}

tasks.jar {
    enabled = true
    dependsOn(tasks.test)
}

tasks.sonarqube {
    dependsOn(tasks.build)
}

sonarqube {
    properties {
        property("sonar.projectKey", "rkuijt_mapper-lib_AX69ZrEpc7fpE6iPeR40")
        property("sonar.qualitygate.wait", true)
        property("sonar.java.coveragePlugin", "jacoco")
        property("sonar.jacoco.reportPaths", "build/reports/jacoco/test/jacocoTestReport.xml")
    }
}

snyk {
    setArguments("--all-sub-projects")
    setSeverity("low")
    setAutoDownload(true)
}

java {
    withSourcesJar()
    withJavadocJar()
}

signing {
    useGpgCmd()
    setRequired { !isSnapshot && gradle.taskGraph.allTasks.any { it is PublishToMavenRepository } }
    sign(publishing.publications)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = "mapper-lib"
            from(components["java"])
            pom {
                name.set("mapper-lib")
                description.set("Clean, lightweight and simple mapping library.")
                url.set("https://mapper-lib.rkuijt.nl/")
                licenses {
                    license {
                        name.set("MIT License")
                        url.set("https://opensource.org/licenses/MIT")
                    }
                }
                developers {
                    developer {
                        id.set("rkuijt")
                        name.set("Robin Kuijt")
                        email.set("robin.kuijt@hu.nl")
                        organizationUrl.set("https://github.com/rkuijt")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com:rkuijt/mapper-lib.git")
                    developerConnection.set("scm:git:ssh://gitlab.com:rkuijt/mapper-lib.git")
                    url.set("https://gitlab.com/rkuijt/mapper-lib/")
                }
            }
        }
    }

    repositories {
        maven {
            val releasesRepoUrl = "https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/"
            val snapshotsRepoUrl = "https://s01.oss.sonatype.org/content/repositories/snapshots/"
            url = uri(if (isSnapshot) snapshotsRepoUrl else releasesRepoUrl)
            credentials {
                username = findProperty("ossrhUsername") as String?
                password = findProperty("ossrhPassword") as String?
            }
        }
    }
}
